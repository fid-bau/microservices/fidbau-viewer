import "@webcomponents/webcomponentsjs/custom-elements-es5-adapter";
import { Vector3, Group } from "three";
import Viewer from "./viewer";
const css = require("./viewer.css").toString()

export default class ViewerElement extends HTMLElement {
    private viewer: Viewer
    private root: ShadowRoot
    private exclusiveModelIndex = 0

    static get observedAttributes() {
        return ["src", "stats", "highlight"];
    }

    constructor() {
        super()
        this.root = this.attachShadow({mode: "open"});
        const style = document.createElement("style")
        style.textContent = css
        this.root.appendChild(style)
        this.viewer = new Viewer(this.root)

        const posterURL = this.getAttribute("poster")
        if (posterURL) {
            const poster = document.createElement("img")
            poster.classList.add("poster")
            poster.setAttribute("src", posterURL)
            poster.addEventListener("mouseenter", () => {
                poster.addEventListener("transitionend", () => {
                    poster.remove()
                    this.forceApplyAttributes()
                    this.applySubElements()
                })
                poster.classList.add("fade-out")
            })
            this.root.appendChild(poster)
        }
        else {
            this.applySubElements()
        }
    }

    public connectedCallback(): void {
        new ResizeObserver(() => {
            this.viewer.setSize(this.clientWidth, this.clientHeight)
        }).observe(this)
    }

    public disconnectedCallback(): void { }

    public attributeChangedCallback(name: string, oldValue: any, newValue: any): void {
        // console.log('--- attributeChangedCallback', name, oldValue, newValue, this.getAttribute(name))
        if (!this.root.querySelector(".poster")) {
            if (name === "stats") {
                this.viewer.setStats(newValue !== null)
            }
            else if (name === "src") {
                this.viewer.removeObjectBySource(oldValue)
                this.loadModelFromElement(this, true)
            }
        }
    }

    private forceApplyAttributes() {
        for (let att of ViewerElement.observedAttributes) {
            this.attributeChangedCallback(att, null, this.getAttribute(att))
        }
    }

    private applySubElements() {
        this.querySelectorAll("source:not([exclusive])").forEach(source => {
            this.loadModelFromElement(source)
        })
        const exclusiveModels = this.querySelectorAll("source[exclusive]")
        if (exclusiveModels.length > 0) {
            this.loadModelFromElement(exclusiveModels.item(this.exclusiveModelIndex))
            if (exclusiveModels.length > 1) {
                window.addEventListener("keyup", event => {
                    if (event.code === "ArrowLeft") {
                        this.loadNextExclusiveModel()
                    }
                })
            }
        }
    }

    private loadNextExclusiveModel() {
        const exclusiveModels = this.querySelectorAll("source[exclusive]")
        const previousExclusiveModelURL = (exclusiveModels.item(this.exclusiveModelIndex)).getAttribute("src")
        this.exclusiveModelIndex = (this.exclusiveModelIndex + 1) % (exclusiveModels.length)
        console.log("--- next index", this.exclusiveModelIndex)
        this.loadModelFromElement(exclusiveModels.item(this.exclusiveModelIndex)).then(() => {
            if (previousExclusiveModelURL) {
                this.viewer.removeObjectBySource(previousExclusiveModelURL)
            }
        })
    }

    private loadModelFromElement(elem: Element, fitCamera = false): Promise<Group | void> {
        const srcURL = elem.getAttribute("src")
        if (srcURL) {
            const src = srcURL.split('#')[0]
            const hash = srcURL.split('#')[1]
            return this.viewer.load(src, elem.getAttribute("type") || undefined).then(model => {
                if (model) {
                    if (fitCamera) {
                        this.viewer.fitCameraToObject(model)
                    }
                    const highlightID = elem.getAttribute("highlight") || hash
                    if (highlightID) {
                        let highlighted = this.viewer.highlightObject(model, highlightID)
                        if (highlighted && fitCamera) {
                            this.viewer.fitCameraToObject(highlighted)
                        }
                    }
                    applyTransformFromElement(elem, model)
                }
                return model
            })
        }
        return Promise.resolve()
    }
}

function applyTransformFromElement(elem: Element, model: Group) {
    const pos = stringToFloatArray(elem.getAttribute("pos"))
    if (pos.length === 3) {
        model.matrix.setPosition(pos[0], pos[1], pos[2])
    }
    const rot = stringToFloatArray(elem.getAttribute("rot"))
    if (rot.length === 3) {
        model.rotation.set(rot[0] * Math.PI / 180, rot[1] * Math.PI / 180, rot[2] * Math.PI / 180)
    }
    const scale = stringToFloatArray(elem.getAttribute("scale"))
    if (scale.length === 3) {
        model.scale.set(scale[0], scale[1], scale[2])
    }
}

function stringToFloatArray(input: string | null): Array<number> {
    const result = new Array<number>()
    if (input) {
        for (let coord of input.split(",")) {
            result.push(parseFloat(coord))
        }
    }
    return result
}

customElements.define('fidbau-viewer', ViewerElement);
