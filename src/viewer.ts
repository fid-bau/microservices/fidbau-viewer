import { Scene, Clock, AmbientLight, PerspectiveCamera, PointLight, DirectionalLight, WebGLRenderer, MeshLambertMaterial, HemisphereLight, Group, Box3, Vector3, sRGBEncoding, Mesh, AxesHelper, LoadingManager, Material, Object3D } from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import FlyControls from './fly-controls'
import Stats from 'three/examples/jsm/libs/stats.module'
import { load, createLoadingManager } from './loaders'

export default class Viewer {
    private clock = new Clock();
    private scene = new Scene();
    private orbitCamera = new PerspectiveCamera(30, 1, 0.1, 1000);
    private flyCamera = new PerspectiveCamera(30, 1, 0.1, 1000);
    private renderer = new WebGLRenderer({ antialias: true });
    private orbitControls: OrbitControls;
    private flyControls: FlyControls;
    private stats: Stats | undefined;
    private loadingManager: LoadingManager;
    private root: ShadowRoot;
    private orbit = true;
    private animating = false;
    private loadedModels: Record<string, Group> = {}
    private activeObject: Object3D | undefined = undefined
    // private highlightedObjectName: string | null = null

    constructor(root: ShadowRoot) {
        this.root = root

        this.renderer.setPixelRatio(window.devicePixelRatio)
        this.renderer.outputEncoding = sRGBEncoding
        this.renderer.physicallyCorrectLights = true
        this.renderer.shadowMap.enabled = true
        this.renderer.setClearColor(0xffffff , 0)
        root.appendChild(this.renderer.domElement)

        this.loadingManager = createLoadingManager(root)

        this.orbitControls = new OrbitControls(this.orbitCamera, this.renderer.domElement);
        this.orbitControls.enableDamping = true
        // this.orbitControls.autoRotate = true
        this.flyControls = new FlyControls(this.flyCamera, this.renderer.domElement);
        this.flyControls.dragToLook = true
        this.flyControls.movementSpeed = 20
        this.flyControls.rollSpeed = 0.2

        this.initLights()
        this.createModeToggleButton()
        this.updateControlMode(true)
    }

    private initLights(): void {
        this.scene.add(new AmbientLight(0x383838, 5))

        const hemiLight = new HemisphereLight( 0xffffff, 0xffffff, 0.6 );
        hemiLight.color.setHSL( 0.6, 1, 0.6 );
        hemiLight.groundColor.setHSL( 0.095, 1, 0.75 );
        hemiLight.position.set( 0, 500, 0 );
        this.scene.add( hemiLight );

        const pointLight = new PointLight( 0xffffff , 10);
        pointLight.castShadow = true
        // this.camera.add(pointLight);

        var dirLight = new DirectionalLight( 0xffffff, 1 );
        dirLight.position.set( -1, 0.75, 1 );
        dirLight.position.multiplyScalar( 50);
        dirLight.name = "dirlight";

        // dirLight.castShadow = true;
        // dirLight.shadow.mapSize.width = dirLight.shadow.mapSize.height = 1024*2;

        // var d = 300;

        // dirLight.shadow.camera.left = -d;
        // dirLight.shadow.camera.right = d;
        // dirLight.shadow.camera.top = d;
        // dirLight.shadow.camera.bottom = -d;

        // dirLight.shadow.camera.far = 3500;
        // dirLight.shadow.bias = -0.0001;
        this.scene.add( dirLight );
    }

    private updateControlMode(nowOrbitting: boolean) {
        if (this.orbit && !nowOrbitting) {
            this.flyControls.activate()
        } else if (!this.orbit && nowOrbitting) {
            this.flyControls.dispose()
        }
        this.orbit = nowOrbitting
    }

    public setStats(enabled: boolean): void {
        if (enabled && !this.stats) {
            this.stats = Stats()
            this.stats.dom.style.position = "absolute"
            this.root.appendChild(this.stats.dom)
        } else if (!enabled && this.stats) {
            this.root.removeChild(this.stats.dom)
            this.stats = undefined
        }
    }

    public setSize(width:number, height:number): void {
        this.flyCamera.aspect = width / height
        this.flyCamera.updateProjectionMatrix()
        this.orbitCamera.aspect = width / height
        this.orbitCamera.updateProjectionMatrix()
        this.renderer.setSize(width, height)
        this.render()
    }

    public load(url: string, type?: string): Promise<Group | void> {
        return load(url, this.loadingManager, type).then((group) => {
            group.traverse(function (child) {
                if ((child as Mesh).isMesh) {
                    const m = child as Mesh
                    m.receiveShadow = true
                    m.castShadow = true
                }
                // if ((child as THREE.Light).isLight) {
                //     const l = child as THREE.Light
                //     l.castShadow = true
                //     l.shadow.bias = -0.003
                //     l.shadow.mapSize.width = 2048
                //     l.shadow.mapSize.height = 2048
                // }
            })
            this.loadedModels[url] = group
            this.scene.add(group)

            if (!this.animating) {
                this.animating = true
                this.animate()
            }
            return group
        }).catch((e) => {
            console.log(e)
            const errorMessage = document.createElement("div")
            errorMessage.classList.add("error-message")
            errorMessage.innerHTML = e
            this.root.appendChild(errorMessage)
        })
    }

    public highlightObject(model: Group, name: string | null, fitCamera = false): (Object3D | null) {
        let result: Object3D | null = null
        model.traverse(child => {
            const mesh = child as Mesh
            if (mesh.isMesh) {
                if (child.name === name && !mesh.userData["material"]) {
                    const material = new MeshLambertMaterial({ color: 0x0000ff, emissive: 0 })
                    // save original material so that it can be restored upon unselection
                    mesh.userData["material"] = mesh.material
                    mesh.material = material
                    result = mesh
                }
                else if (child.name !== name && mesh.userData["material"]) {
                    // check if an object was selected before and needs to be unselected / reset material to original
                    mesh.material = mesh.userData["material"]
                }
            }
        })
        return result
    }

    public removeObjectBySource(url: string) {
        let object = this.loadedModels[url]
        if (object) {
            object.clear().removeFromParent()
        }
        delete this.loadedModels[url]
    }

    public reset() {
        this.animating = false
        for (let key in this.loadedModels) {
            this.removeObjectBySource(key)
        }
    }

    public animate() {
        if (this.animating) {
            requestAnimationFrame(() => this.animate())
            this.render()
        }
    }
    
    public render() {
        if (this.orbit) {
            this.orbitControls.update()
            this.renderer.render(this.scene, this.orbitCamera)
        } else {
            this.flyControls.update(this.clock.getDelta())
            this.renderer.render(this.scene, this.flyCamera)
        }
        if (this.stats) {
            this.stats.update()
        }
    }

    public fitCameraToObject(object: Object3D, fitOffset: number = 1.2) {
        this.activeObject = object
        const bounds = new Box3().expandByObject(object).applyMatrix4(object.modelViewMatrix)
        const size = bounds.getSize(new Vector3())
        const center = bounds.getCenter(new Vector3())
        
        const maxSize = Math.max(size.x, size.y, size.z)
        const fitHeightDistance = maxSize / (2 * Math.atan(Math.PI * (this.orbit ? this.orbitCamera.fov : this.flyCamera.fov) / 360))
        const fitWidthDistance = fitHeightDistance / (this.orbit ? this.orbitCamera.aspect : this.flyCamera.aspect)
        const distance = fitOffset * Math.max(fitHeightDistance, fitWidthDistance)
        // const direction = this.controls.target.clone().sub(this.camera.position).normalize().multiplyScalar(distance)
        const direction = new Vector3(0, 0, -1).multiplyScalar(distance)
      
        if (this.orbit) {
            this.orbitControls.maxDistance = distance * 10
            this.orbitControls.target.copy(center)
            this.orbitControls.update()
            
            this.orbitCamera.near = distance / 100
            this.orbitCamera.far = distance * 100
            this.orbitCamera.updateProjectionMatrix()
            this.orbitCamera.position.copy(center).sub(direction)
            this.orbitCamera.lookAt(center)
        } else {
            this.flyCamera.near = distance / 100
            this.flyCamera.far = distance * 100
            this.flyCamera.updateProjectionMatrix()
            this.flyCamera.position.copy(center).sub(direction)
            this.flyCamera.lookAt(center)
        }
    }

    private createModeToggleButton() {
        const button = document.createElement("button")
        button.setAttribute("type", "button")
        button.classList.add("mode-toggle")
        button.textContent = this.orbit ? "Fly" : "Orbit"
        button.onclick = () => {
            this.updateControlMode(!this.orbit)
            button.textContent = this.orbit ? "Fly" : "Orbit"
            if (this.activeObject) {
                this.fitCameraToObject(this.activeObject)
            }
        }
        this.root.appendChild(button)
    }
}