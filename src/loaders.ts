import { Euler, Mesh, LoadingManager, Group, MeshLambertMaterial } from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'
import { IFCLoader } from 'three/examples/jsm/loaders/IFCLoader'
import { STLLoader } from 'three/examples/jsm/loaders/STLLoader'

const suffixToType: Record<string, string> = {
    "gltf": "gltf",
    "glb": "gltf",
    "stl": "stl",
    "ifc": "ifc",
}
const loaders: Record<string, Function> = {
    "gltf": loadGLTF,
    "ifc": loadIFC,
    "stl": loadSTL,
}

export function createLoadingManager(root: ShadowRoot): LoadingManager {
    const manager = new LoadingManager()
    manager.onStart = () => {
        let screen = root.querySelector(".loading-screen")
        if (!screen) {
            screen = document.createElement("div")
            screen.classList.add("loading-screen")
            screen.innerHTML = "<div class='spinner'></div>"
            root.appendChild(screen)
        }
        screen.classList.remove("hidden", "fade-out")
    }
    manager.onLoad = () => {
        const screen = root.querySelector(".loading-screen")
        if (screen) {
            screen.addEventListener("transitionend", () => { screen.classList.add("hidden") })
            screen.classList.add("fade-out")
        }
    }
    manager.onError = (e) => { console.error("loading failed", e)}
    return manager
}

export function load(url: string, manager: LoadingManager, type?: string): Promise<Group> {
    if (!type) {
        const suffix = url.split(/[#?]/)[0].split('.').pop()
        if (suffix) {
            type = suffixToType[suffix.trim()]
        }
    }
    if (!type) {
        return Promise.reject("couldn't infer type from url " + url)
    }
    const loader = loaders[type]
    if (!loader) {
        return Promise.reject("no loader for type " + type)
    }
    return loader(manager, url)
}

function loadGLTF(manager: LoadingManager, url:string): Promise<Group> {
    const loader = new GLTFLoader(manager)
    loader.setDRACOLoader(new DRACOLoader().setDecoderPath('https://data.fid-bau.de/fid/js/'))
    return loader.loadAsync(url).then((gltf) => { return gltf.scene })
}

function loadSTL(manager: LoadingManager, url:string): Promise<Group> {
    const loader = new STLLoader(manager)
    return loader.loadAsync(url).then((geometry) => {
        const material = new MeshLambertMaterial({ color: 0xffffff, emissive: 0 })
        const group = new Group().add(new Mesh(geometry, material))
        group.quaternion.setFromEuler(new Euler(-Math.PI / 2, 0, 0))
        return group
    })
}

function loadIFC(manager: LoadingManager, url:string): Promise<Group> {
    return Promise.reject("ifc not working currently")
}